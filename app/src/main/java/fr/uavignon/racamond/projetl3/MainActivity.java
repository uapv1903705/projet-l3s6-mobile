package fr.uavignon.racamond.projetl3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity
{
    private ListView listView;
    private Spinner spinnerOrder;
    private EditText searchInput;
    private MuseeDataHandler mdh;
    private MuseeAdapter.SortType sortType;
    private MuseeAdapter museeAdapter;
    private SearchFilter searchFilter;
    private ImageView downloading;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mdh = new MuseeDataHandler(this);
        listView = findViewById(R.id.listView);
        downloading = findViewById(R.id.main_downloading);
        listView.setVisibility(View.GONE);
        downloading.setVisibility(View.VISIBLE);
        initSearchOrder();
        initSpinnerOrder();
    }

    // hook the searchbar to update the searchfilter each change.
    public void initSearchOrder()
    {
        searchInput = findViewById(R.id.searchInput);
        searchFilter = new SearchFilter();
        searchInput.addTextChangedListener(new TextWatcher()
        {
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
            public void afterTextChanged(Editable s)
            {
                updateSearchValue();
            }
        });
    }

    // hook the spinner to change the sort type and update when needed.
    public void initSpinnerOrder()
    {
        spinnerOrder = findViewById(R.id.sortSpinner);
        spinnerOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                MuseeAdapter.SortType newSort = sortType;
                switch(position)
                {
                    case 0: newSort = MuseeAdapter.SortType.CATEGORY; break;
                    case 1: newSort = MuseeAdapter.SortType.YEAR; break;
                    case 2: newSort = MuseeAdapter.SortType.ALPHABETICAL; break;
                }
                if(!newSort.equals(sortType))
                {
                    sortType = newSort;
                    updateList();
                }
            }
            public void onNothingSelected(AdapterView<?> adapterView){}
        });
    }

    // reload the list with the new information
    public void updateList()
    {
        if(!mdh.hasLoaded())
            return;

        listView.setVisibility(View.VISIBLE);
        downloading.setVisibility(View.GONE);

        museeAdapter = new MuseeAdapter(MainActivity.this, mdh.items, sortType, searchFilter);
        listView.setAdapter(museeAdapter);
    }

    // update the search filter.
    public void updateSearchValue()
    {
        if(!mdh.hasLoaded())
            return;

        searchFilter.set(searchInput.getText().toString());
        museeAdapter.notifyDataSetChanged();    // tell it that the data have changed so it refreshes accordingly
    }

    public void goToItemActivity(String id)
    {
        Intent appInfo = new Intent(MainActivity.this, MuseeItemActivity.class);
        appInfo.putExtra("id", id);
        startActivity(appInfo);
    }

    // a network error likely occured, display a no network icon instead of the loading one.
    // TODO: try again to load items when it's back on.
    public void error()
    {
        downloading.setImageResource(R.drawable.nonetwork);
    }
}



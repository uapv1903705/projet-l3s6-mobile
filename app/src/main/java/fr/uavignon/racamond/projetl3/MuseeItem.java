package fr.uavignon.racamond.projetl3;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MuseeItem
{
    // The various fields from the JSON object
    public String id;
    public String name;
    public String brand;
    public String description;
    public boolean working;
    public ArrayList<String> categories;
    public ArrayList<String> technicalDetails;
    public int year;
    public int[] timeframe;
    public Date demoDate;
    private Drawable thumbnail;

    // Clone, I need to copy items for categories.
    public MuseeItem(MuseeItem item)
    {
        this.id = item.id;
        this.name = item.name;
        this.brand = item.brand;
        this.year = item.year;
        this.timeframe = item.timeframe;
        this.thumbnail = item.thumbnail;
        if(item.categories!=null){this.categories = new ArrayList<>();for (String s : item.categories)this.categories.add(s);}
        if(item.technicalDetails!=null){this.technicalDetails = new ArrayList<>();for (String s : item.technicalDetails)this.technicalDetails.add(s);}
        this.working = item.working;
        this.description = item.description;
        this.demoDate = item.demoDate;
    }

    // Grab the json and the id, make a MuseeItem out of it.
    public MuseeItem(JSONObject jsonObject, String id)
    {
        try
        {
            this.id = id;
            categories = new ArrayList<>();
            technicalDetails = new ArrayList<>();
            name = setStringFromJSON(jsonObject,"name");
            brand = setStringFromJSON(jsonObject,"brand");
            description = setStringFromJSON(jsonObject,"description");
            categories = setStringArrayFromJSON(jsonObject,"categories");
            technicalDetails = setStringArrayFromJSON(jsonObject,"technicalDetails");
            if(jsonObject.has("year")) year = jsonObject.getInt("year");
            if(jsonObject.has("working")) working = true; else working = false; // doesn't work if the field is missing
            if(jsonObject.has("timeFrame")) // 1980, 1990, 2000..
            {
                JSONArray timeframeJson = jsonObject.getJSONArray("timeFrame");
                timeframe = new int[timeframeJson.length()];
                for (int i = 0; i<timeframeJson.length(); i++)
                    timeframe[i] = timeframeJson.getInt(i);
            }
        }
        catch (Exception e){e.printStackTrace();}
    }

    // for the list, they don't always have a specific year, get the timeframe if they don't etc.. it's for human display.
    public String getYearAny()
    {
        if(year!=0)
            return ""+year;
        else
        {
            if(timeframe == null)
                return "????";
            if(timeframe.length < 2)
                return ""+timeframe[0]+"s";
            return ""+timeframe[0]+"-"+timeframe[timeframe.length-1];
        }
    }

    // Text for the detailed item
    public String getYearText()
    {
        if(year == 0 && timeframe.length == 0)
            return null;

        String text = "";
        if(year!=0)                             // no specific date
            text+=year;

        if(timeframe.length == 1 && year == timeframe[0]){}     // dumb to say 2001 -> years 2001s for instance
        else
        {
            if (timeframe.length > 0 && year != 0)                  // has a timeframe, a year
                text += " ➤ Années ";   // TODO replace with a string resource.
            if (timeframe.length == 1)               // XXXXs
                text += "" + timeframe[0] + "s";
            else if (timeframe.length < 3)          // XXXX-YYYY
                text += "" + timeframe[0] + "-" + timeframe[1];
            else                                    // XXXX, YYYY, ZZZZ
                for (int i = 0; i < timeframe.length; i++)
                {
                    text += timeframe[i];
                    if (i + 1 < timeframe.length)
                        text += ", ";
                }
        }
        return text;
    }

    // Grab the specs, make a single String out of each field
    public String getTechnicalText()
    {
        String text;
        if(technicalDetails == null) return null;
        text = "";
        for (String spec : technicalDetails)
        {
            text += "- "+spec+"\n";
        }
        return text;
    }

    // get the date using a proper human format
    public String getDemoDateText()
    {
        String text;
        text = ""+demoDate.toLocaleString();
        return text;
    }

    public void setThumbnail(Drawable drawable)
    {
        thumbnail = drawable;
    }
    public Drawable getThumbnail()
    {
        return thumbnail;
    }

    public String toString()
    {
        return name+" "+brand;
    }

    // False and True aren't very good for humans, grab the translated strings for each
    public String getWorkingResourceString(Activity activity) { return working?activity.getResources().getString(R.string.item_working_true):activity.getResources().getString(R.string.item_working_false); }

    // If it has no year but a timeframe, grab the first date in the timeframe
    public static void sortItemsByYear(List<MuseeItem> items)
    {
        Collections.sort(items, new Comparator<MuseeItem>()
        {
            public int compare(MuseeItem a, MuseeItem b)
            {
                int yearA = a.year==0?a.timeframe[0]:a.year;
                int yearB = b.year==0?b.timeframe[0]:b.year;
                return yearA-yearB;
            }
        });
    }

    // Compare the strings to know which is first
    public static void sortItemsByName(List<MuseeItem> items)
    {
        Collections.sort(items, new Comparator<MuseeItem>()
        {
            public int compare(MuseeItem a, MuseeItem b)
            {
                return a.name.compareTo(b.name);
            }
        });
    }

    // ditto
    public static void sortItemsByCategory(List<MuseeItem> items)
    {
        Collections.sort(items, new Comparator<MuseeItem>()
        {
            public int compare(MuseeItem a, MuseeItem b)
            {
                return a.categories.get(0).compareTo(b.categories.get(0));
            }
        });
    }

    // Append everything so I can easily check each field
    public String toFilterCheck(Activity activity)
    {
        return name+"|"+categories+"|"+brand+"|"+year+"|"+timeframe+"|"+getWorkingResourceString(activity);
    }

    // Grab the JSON String as it's redundant to do this each time
    private String setStringFromJSON(JSONObject jsonObject, String jsonString)
    {
        try
        {
            if (!jsonObject.has(jsonString))
                return null;

            String val = jsonObject.getString(jsonString);
            if(!val.isEmpty())
                return jsonObject.getString(jsonString);
        }
        catch (Exception e){e.printStackTrace();}
        return null;
    }

    // Fill the array using the JSONArray.
    private ArrayList<String> setStringArrayFromJSON(JSONObject jsonObject, String jsonString)
    {
        try
        {
            if (!jsonObject.has(jsonString))
                return null;

            ArrayList<String> array = new ArrayList<>();
            JSONArray catJson = jsonObject.getJSONArray(jsonString);
            for (int i = 0; i<catJson.length(); i++)
                array.add(catJson.getString(i));
            return array;
        }
        catch (Exception e){e.printStackTrace();}
        return null;
    }

    // Parse the demo date string
    public void setDemoDate(String s)
    {
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            demoDate = format.parse(s);
        }
        catch (Exception e){e.printStackTrace();}
    }
}

package fr.uavignon.racamond.projetl3;

import androidx.collection.ArrayMap;

import java.util.ArrayList;
import java.util.Map;

// List "adapter" I had to create since Strings are Objects and use references.
public class StringList
{
    ArrayList<String> list;

    public StringList()
    {
        list = new ArrayList<>();
    }

    public boolean add(String s)
    {
        if(exists(s))
            return false;
        list.add(s);
        return true;
    }

    // Can't find the key because it's a ref
    public boolean remove(String s)
    {
        for(String o : list)
            if(o.equals(s))
            {
                list.remove(o);
                return true;
            }
        return false;
    }

    // Remove if it's there, add if not.
    public void toggle(String s)
    {
        for (String o : list)
            if(o.equals(s))
            {
                list.remove(o);
                return;
            }
        list.add(s);
    }

    // Does the key exist? I'm not talking about the pointer, but the value of the String.
    public boolean exists(String s)
    {
        for (String o : list)
            if(o.equals(s))
                return true;
        return false;
    }

    // Gotta debug them arrays am I right?
    public String toString()
    {
        String s = "[ ";
        for (String o: list)
            s+=o+", ";
        s+=" ]";
        return s;
    }

    // Where's my value at? No references please, check by String value.
    public int getPosition(String s)
    {
        for (int i = 0; i<list.size(); i++)
            if(list.get(i).equals(s))
                return i;
        return -1;
    }

    public int size()
    {
        return list.size();
    }

    // Maps are lists, they're a pain for Strings too, I'll need that... but it's not the class's job.. static it is then!
    public static Object getValueFromStringObjectArrayMap(String s, Object arrayMap)
    {
        for(Map.Entry<String, Object> entry : ((ArrayMap<String,Object>)arrayMap).entrySet())
        {
            if(entry.getKey().equals(s))
                return entry.getValue();
        }
        return null;
    }

    // The function's name says it all.
    public static void addIfDoesntExistStringObjectArrayMap(String s, Object o, Object arrayMap)
    {
        for(Map.Entry<String, Object> entry : ((ArrayMap<String,Object>)arrayMap).entrySet())
        {
            if(entry.getKey().equals(s))
                return;
        }
        ((ArrayMap<String,Object>)arrayMap).put(s,o);
    }
}

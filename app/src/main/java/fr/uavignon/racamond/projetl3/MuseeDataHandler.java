package fr.uavignon.racamond.projetl3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import androidx.collection.ArrayMap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

// Class that has all the "data" and do the requests.
public class MuseeDataHandler
{
    static final String urlBase = "https://demo-lia.univ-avignon.fr/cerimuseum/";  // shouldn't change.
    private int receivedAllRequirements = 0;  // Check to see if all the async requests finished
    private boolean doneReceiving = false;  // Am I done receiving the data? Can other classes request stuff yet?
    Activity activity;  // Parent activity to be able to notify about the received data

    private static ArrayList<String> updatingItemIDs;  // Check to know if already updating
    private static ArrayMap<String,Drawable> idThumbnailsMap;   // list of the thumbnails by ID
    private static ArrayMap<String,Drawable> idPicturesMap;     // list of the pictures by ID
    private static ArrayMap<String,String> idPicturesDescMap;   // list of the pictures descriptions by ID

    static ArrayList<String> categories;    // all categories
    static ArrayList<String> ids;           // all item ids
    static ArrayList<MuseeItem> items;      // all items

    // if it's the MainActivity, we'll require the categories and ids, we'll see about the catalog once all received
    public MuseeDataHandler(MainActivity activity)
    {
        this.activity = activity;
        updatingItemIDs = new ArrayList<>();
        idThumbnailsMap = new ArrayMap<>();
        idPicturesDescMap = new ArrayMap<>();
        requestArray("categories");
        requestArray("ids");
    }

    // if it's the MuseeItemHandler, request the clicked item
    public MuseeDataHandler(MuseeItemActivity activity, String itemID)
    {
        this.activity = activity;
        this.idPicturesMap = new ArrayMap<>();
        requestItem(itemID);
    }

    // Request an array of JSON
    @SuppressLint("StaticFieldLeak")
    private void requestArray(String page)
    {
        receivedAllRequirements--;  // go in the negatives, we'll check if it's equal to 0 each time to check if we got everything.
        final String usedPage = page;    // needs to be final, could have done it in the args. TODO: put final in the args and replace the final var
        new AsyncTask<String, Void, String>()
        {
            protected String doInBackground(String... urls)     // Do it in another thread
            {
                String concatURLs = ""; for (String url : urls) concatURLs += url.toString();
                return getFileFromURL(concatURLs);
            }
            protected void onPostExecute(String receivedData)   // Once you come back, do your thing.
            {
                try
                {
                    ArrayList<String> data = new ArrayList<String>();
                    JSONArray jsonArray = new JSONArray(receivedData);
                    for (int i = 0; i < jsonArray.length(); i++)
                        data.add(jsonArray.getString(i));           // Fill the data

                    receiveArray(data, usedPage);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    ((MainActivity)activity).error();       // Something went wrong in the download or something. TODO: I need to handle errors properly, that's dirty
                }
            }
        }.execute(urlBase+page);
    }

    @SuppressLint("StaticFieldLeak")
    private void requestCatalog()
    {
        new AsyncTask<String, Void, String>()
        {
            protected String doInBackground(String... urls) // See requestArray(String)
            {
                String concatURLs = ""; for (String url : urls) concatURLs += url.toString();
                return getFileFromURL(concatURLs);
            }
            protected void onPostExecute(String receivedData)
            {
                try
                {
                    items = new ArrayList<MuseeItem>();
                    JSONObject root = new JSONObject(receivedData);

                    for (String id : ids)
                        items.add(new MuseeItem(root.getJSONObject(id), id));

                    doneReceivingCatalog();     // I've got it, now we can continue
                }
                catch (Exception e) {e.printStackTrace();}
            }
        }.execute(urlBase+"catalog");
    }

    @SuppressLint("StaticFieldLeak")
    private void requestItem(final String itemID)   // I just need a single item
    {
        new AsyncTask<String, Void, String>()
        {
            protected String doInBackground(String... urls)
            {
                String concatURLs = ""; for (String url : urls) concatURLs += url;
                return getFileFromURL(concatURLs);
            }
            protected void onPostExecute(String receivedData)
            {
                try
                {
                    JSONObject itemJson = new JSONObject(receivedData);
                    StringList picturesArray = new StringList();
                    if(itemJson.has("pictures"))                // try to grab all the pictures
                    {
                        JSONObject picturesDict = itemJson.getJSONObject("pictures");
                        String key = "";
                        Iterator<String> it = picturesDict.keys();
                        while(it.hasNext())         // Iterate through the JSONObject, get all the keys, it's not an ArrayMap, but kinda is.. so treat it as such.
                        {
                            key = it.next();
                            picturesArray.add(key);
                            if(StringList.getValueFromStringObjectArrayMap(key,idPicturesMap)==null)    // String are references, need to check the value instead
                                requestPicture(itemID, key, picturesDict.has(key)?picturesDict.getString(key):null);
                            else
                                ((MuseeItemActivity)activity).pictureReceived(key);
                        }
                    }
                    ((MuseeItemActivity)activity).receiveItem(new MuseeItem(itemJson,itemID), picturesArray);
                }
                catch (Exception e) {e.printStackTrace();}
            }
        }.execute(urlBase+"items/"+itemID);
    }


    // Request a picture, I request them one by one, last the we want is to make the user wait until everything loads
    @SuppressLint("StaticFieldLeak")
    private void requestPicture(final String itemID, final String pictureID, final String description)
    {
        new AsyncTask<String, Void, String>()
        {
            protected String doInBackground(String... urls)
            {
                String concatURLs = ""; for (String url : urls) concatURLs += url;
                try
                {
                    String key = ""+itemID+"-picture-"+pictureID;       // format its name in case an item has the same picture name
                    InputStream is = (InputStream) new URL(concatURLs).getContent();
                    Drawable pic = Drawable.createFromStream(is, key);
                    StringList.addIfDoesntExistStringObjectArrayMap(key,pic,idPicturesMap);     // String are objects, gotta do it by value, Java doesn't has a function for that.
                    StringList.addIfDoesntExistStringObjectArrayMap(key,description,idPicturesDescMap);
                    return pictureID;
                }
                catch (Exception e) {e.printStackTrace();}
                return null;
            }
            protected void onPostExecute(String pictureID)
            {
                ((MuseeItemActivity)activity).pictureReceived(pictureID);   // Notify that it was received so it can grab it
            }
        }.execute(urlBase+"items/"+itemID+"/images/"+pictureID);
    }

    @SuppressLint("StaticFieldLeak")
    private void requestDemos()     // Request the demo dates
    {
        new AsyncTask<String, Void, Void>()
        {
            protected Void doInBackground(String... urls)
            {
                String concatURLs = ""; for (String url : urls) concatURLs += url;
                try
                {
                    JSONObject jsonDates = new JSONObject(getFileFromURL(concatURLs));
                    String key;
                    Iterator<String> it = jsonDates.keys();
                    while(it.hasNext())     // Not an array but dictionary, grab the date using the item's id
                    {
                        key = it.next();
                        getItemFromID(key).setDemoDate(jsonDates.getString(key));
                    }
                }
                catch (Exception e) {e.printStackTrace();}
                return null;
            }
            protected void onPostExecute(Void voids)
            {
                doneReceivingDemos();
            }   // got them
        }.execute(urlBase+"demos");

    }

    // assign the data depending on the array requested TODO: perhaps I should pass the array directly as it's always strings instead of doing a switch by String page
    private void receiveArray(ArrayList<String> data, String page)
    {
        switch (page)
        {
            case "categories": categories = data; break;
            case "ids": ids = data; break;
        }
        doneReceivingRequirements();    // I'm done getting every array, we can get to business
    }

    // Get the data from the website, I should only call it when in a different thread, android wont like it if I don't as it'd locks the app.
    private String getFileFromURL(String path)
    {
        try
        {
            URL url = new URL(path);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = urlConnection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            for (String line; (line = r.readLine()) != null; )      // Put it all lines in a string
                result.append(line);

            return result.toString();
        }
        catch (Exception e)
        {
            e.getStackTrace();
        }
        return null;
    }

    // Grab a specific picture from a specific item
    public static Drawable getPicture(MuseeItem item, String picName)
    {
        // TODO: I should really make a pattern instead of doing it in the code, what if I change the -picture- to image?
        Drawable picture = (Drawable)StringList.getValueFromStringObjectArrayMap(""+item.id+"-picture-"+picName, idPicturesMap);
        return picture;
    }

    public static String getPictureDescription(MuseeItem item, String picName)
    {
        // TODO: ditto
        String description = (String)StringList.getValueFromStringObjectArrayMap(""+item.id+"-picture-"+picName, idPicturesDescMap);
        return description;
    }

    @SuppressLint("StaticFieldLeak")
    public static void assignThumbnail(final String page, final String imgName, final MuseeItem item, final MuseeAdapter adapter)
    {
        if (updatingItemIDs.contains(item.id))
            return;
        if(StringList.getValueFromStringObjectArrayMap(item.id,idThumbnailsMap)!=null)  // Strings are objects, it wont find it if I do .get().
        {
            item.setThumbnail((Drawable)StringList.getValueFromStringObjectArrayMap(item.id,idThumbnailsMap));
            if(adapter!=null)adapter.notifyDataSetChanged();    // If I don't have an adapter, that'd be hard to ask it
            return;
        }

        new AsyncTask<String, Void, Drawable>()
        {
            protected Drawable doInBackground(String... urls)
            {
                String concatURLs = ""; for (String url : urls) concatURLs += url.toString();
                try
                {
                    InputStream is = (InputStream) new URL(concatURLs).getContent();    // grab the image
                    Drawable d = Drawable.createFromStream(is, imgName);    // make a drawable out of it
                    return d;
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    return null;
                }
            }
            protected void onPostExecute(Drawable receivedData)
            {
                try
                {
                    item.setThumbnail(receivedData);
                    StringList.addIfDoesntExistStringObjectArrayMap(item.id,item.getThumbnail(),idThumbnailsMap);   // add the image to the map
                    if(adapter!=null)adapter.notifyDataSetChanged();
                    updatingItemIDs.remove(item.id);
                }
                catch (Exception e) {e.printStackTrace();}
            }
        }.execute(urlBase+page);
        updatingItemIDs.add(item.id);
    }

    // I've got all the arrays and stuff
    private void doneReceivingRequirements()
    {
        receivedAllRequirements++;
        if(receivedAllRequirements==0)  // negative when I request, +1 when I get the data, if 0 then we have received it all.
            requestCatalog();   // We can get the catalog now.
    }

    // Get a specific item from its id
    public MuseeItem getItemFromID(String id)
    {
        for (MuseeItem item: items)
            if(item.id.equals(id))
                return item;
        return null;
    }

    // Got the catalog, we can get the demos now... could have done it before the catalog, but needed the item ids, would need a refactor.. TODO: refactor
    private void doneReceivingCatalog()
    {
        requestDemos();
    }

    // We have the demos, update the list so we can display items and allow clicks on them to get to details
    private void doneReceivingDemos()
    {
        doneReceiving = true;
        ((MainActivity)activity).updateList();
    }

    // Have I loaded everything?
    public boolean hasLoaded()
    {
        return doneReceiving;
    }
}

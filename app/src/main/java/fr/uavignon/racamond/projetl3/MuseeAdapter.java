package fr.uavignon.racamond.projetl3;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;

public class MuseeAdapter extends BaseAdapter
{
    // Added to prevent using integers as indexes as it's messy.
    public enum SortType
    {
        YEAR,
        CATEGORY,
        ALPHABETICAL
    }
    // Holds the content of the item's layout
    public static class ViewHolder
    {
        public TextView name;
        public TextView brand;
        public TextView year;
        public ImageView thumbnail;
        public ConstraintLayout container;
        public TextView category;
        public ConstraintLayout category_container;
        public ImageView category_arrow;
    }

    private Activity activity;                  // parent's activity
    private ArrayList<MuseeItem> itemList;      // the list of the current items, taken from the MDH
    private StringList enabledCategories;       // if a category is enabled from the category sort type, put it there.
    private static LayoutInflater inflater;     // the layout inflater
    private SortType sort;                      // the current sorting type
    private SearchFilter filter;                // the current search filter

    public MuseeAdapter(Activity activity, ArrayList<MuseeItem> itemList, SortType sort, SearchFilter filter)
    {
        this.activity = activity;
        this.enabledCategories = new StringList();
        this.filter = filter;
        if(sort!=SortType.CATEGORY)     // If using the category sort type, we'll need to modify the list. TODO: doesn't this modify the original list? Likely a fail.
            this.itemList = itemList;
        this.sort = sort;
        MuseeAdapter.inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(sort==null)   // TODO: fix the leak, likely due to reloading from android studio instead of properly from the device.
            return;


        switch (sort)   // work on the items accordingly.
        {
            case YEAR: MuseeItem.sortItemsByYear(itemList); break;          // Sort yearly
            case ALPHABETICAL: MuseeItem.sortItemsByName(itemList); break;  // Sort alphabetically
            case CATEGORY:                                                  // Sort by category
                ArrayList<MuseeItem> itemListTemp = new ArrayList<>();
                for (MuseeItem item : itemList)                             // We'll copy each item to have only one category, that way it's displayed multiple times throughout categories.. it just works.
                {
                    for (String cat : item.categories)
                    {
                        MuseeItem newItem = new MuseeItem(item);
                        newItem.categories.clear();
                        newItem.categories.add(cat);
                        itemListTemp.add(newItem);
                    }
                }
                MuseeItem.sortItemsByCategory(itemListTemp);
                this.itemList = itemListTemp;
                break;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        ViewHolder holder;

        if(convertView==null)       // inflate and assign the viewholder
        {
            vi = inflater.inflate(R.layout.musee_list_layout, null);

            holder=new ViewHolder();
            holder.name = vi.findViewById(R.id.museeList_name);
            holder.brand = vi.findViewById(R.id.museeList_brand);
            holder.year = vi.findViewById(R.id.museeList_year);
            holder.thumbnail = vi.findViewById(R.id.museeList_thumbnail);
            holder.category = vi.findViewById(R.id.museeList_category);
            holder.category_container = vi.findViewById(R.id.museeList_category_container);
            holder.category_arrow = vi.findViewById(R.id.museeList_category_arrow);
            holder.container = vi.findViewById(R.id.museeList_container);

            vi.setTag(holder);
        }
        else
            holder=(ViewHolder)vi.getTag();

        boolean matchFilter = true;         // Will put it to false if it doesn't match, need more data first.
        MuseeItem item = itemList.get(position);
        if(sort == SortType.CATEGORY && !enabledCategories.exists(item.categories.get(0)))
        {
            // Don't waste data if the categories are closed.
        }
        else
        {
            if(filter.checkMatch(item.toFilterCheck(activity)))     // display the item if matches, don't know yet about if I need to hide the category.
            {
                holder.container.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.container.setVisibility(View.GONE);
                matchFilter = false;
            }


            holder.name.setText(item.name);
            holder.brand.setText(item.brand);
            holder.year.setText(item.getYearAny());
            holder.category_container.setVisibility(View.GONE);
            if(matchFilter)
            {
                if (item.getThumbnail() == null)        // Request a thumbnail if it's missing, don't try anything if hidden.
                {
                    holder.thumbnail.setImageDrawable(new BitmapDrawable(""));
                    MuseeDataHandler.assignThumbnail("/items/" + item.id + "/thumbnail", "thumbnail-" + item.id, item, this);
                }
                else
                    holder.thumbnail.setImageDrawable(item.getThumbnail());
            }
        }

        if(sort == SortType.CATEGORY)
        {
            if(!enabledCategories.exists(item.categories.get(0)))       // if the category isn't open, hide the thing.
                 holder.container.setVisibility(View.GONE);
            else if(matchFilter)
                holder.container.setVisibility(View.VISIBLE);

            // if it's the first possible item of a listed category and isn't hidden, set as the item that has the category button.
            if((position == 0 || (!itemList.get(position-1).categories.get(0).equals(item.categories.get(0)))) && containItemMatchingFilterAndCategory(item.categories.get(0)))
                setCategoryButton(holder, item);
            else
                holder.category_container.setVisibility(View.GONE);
        }

        // assign the click listener to the item, but only if visible
        if(holder.container.getVisibility() == View.VISIBLE)
        {
            final String itemID = item.id;
            holder.container.setOnClickListener(new View.OnClickListener()
            {
                public void onClick(View view)
                {
                    ((MainActivity)activity).goToItemActivity(itemID);
                }
            });
        }

        return vi;
    }

    // The item has the category button, set all the required stuff.
    public void setCategoryButton(final ViewHolder holder, final MuseeItem item)
    {
        if(enabledCategories.exists(item.categories.get(0)))    // Turned to right when closed, down when open
             holder.category_arrow.setRotation(90);
        else holder.category_arrow.setRotation(0);

        holder.category_container.setVisibility(View.VISIBLE);
        holder.category.setText(item.categories.get(0));
        holder.category_container.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                enabledCategories.toggle(item.categories.get(0));
                notifyDataSetChanged();
            }
        });
    }

    // Is there an item matching the category?
    public boolean containItemMatchingFilterAndCategory(String cat)
    {
        int found = 0;
        for(MuseeItem item : itemList)
        {
            if(item.categories.get(0).equals(cat) && filter.checkMatch(item.toFilterCheck(activity)))
                found++;
        }
        return found!=0?true:false;
    }

    public int getCount() { return itemList.size(); }
    public Object getItem(int position) { return position; }
    public long getItemId(int position) { return position; }
}
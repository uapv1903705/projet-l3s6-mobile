package fr.uavignon.racamond.projetl3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class MuseeItemActivity extends AppCompatActivity
{
    MuseeDataHandler mdh;
    MuseeItem item;
    LinearLayout museeItem_scrollLayout;
    ScrollView museeItem_content;

    ViewGroup.LayoutParams defaultImageLayoutParams;

    ArrayList<ImageAndDescription> pictures;
    StringList listImagesFilenames;
    Drawable missingImage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musee_item);

        museeItem_scrollLayout = findViewById(R.id.museeItem_scrollLayout);
        museeItem_content = findViewById(R.id.museeItem_content);
        museeItem_content.setVisibility(View.GONE);

        String itemID = getIntent().getStringExtra("id");

        mdh = new MuseeDataHandler(this, itemID);
    }


    public void receiveItem(MuseeItem item, StringList listImagesFilenames)
    {
        this.item = item;
        this.listImagesFilenames = listImagesFilenames;

        item.demoDate = mdh.getItemFromID(item.id).demoDate;    // We need it, have it, but not in this received item. Duct Tape striked again.

        initPictures();
        initItem();
        museeItem_content.setVisibility(View.VISIBLE);
    }

    public void initItem()
    {
        // Just put it in the title, it's fancy.
        ((TextView)findViewById(R.id.museeItem_name)).setText(item.name);

        // I need to hide the fields if some details are missing, nobody likes empty fields right?

        if(item.brand!=null) ((TextView)findViewById(R.id.museeItem_brand)).setText(item.brand);
        else (findViewById(R.id.museeItem_brand_layout)).setVisibility(View.GONE);

        if(item.description!=null) ((TextView)findViewById(R.id.museeItem_description)).setText(item.description);
        else (findViewById(R.id.museeItem_description_layout)).setVisibility(View.GONE);

        if(item.technicalDetails!=null) ((TextView)findViewById(R.id.museeItem_technical)).setText(item.getTechnicalText());
        else (findViewById(R.id.museeItem_technical_layout)).setVisibility(View.GONE);

        if(item.year!=0||item.timeframe.length!=0)
        {
            ((TextView)findViewById(R.id.museeItem_year)).setText(item.getYearText());
            if(item.year == 0)      // Not a specific year, so add an S to "year"
                ((TextView)findViewById(R.id.museeItem_year_label)).setText(getResources().getString(R.string.item_years));
        }
        else (findViewById(R.id.museeItem_year)).setVisibility(View.GONE);

        if(item.demoDate!=null) ((TextView)findViewById(R.id.museeItem_demo)).setText(item.getDemoDateText());
        else (findViewById(R.id.museeItem_demo_layout)).setVisibility(View.GONE);

        if(item.working==true) ((TextView)findViewById(R.id.museeItem_working)).setText(getResources().getString(R.string.item_working_true)); // it's already set to false by default
    }

    // Initialize the pictures, so we know that they're there, we have the descriptions, but pictures need to be downloaded, set a missing image
    public void initPictures()
    {
        ImageView defImage = findViewById(R.id.museeItem_defaultImage);
        defaultImageLayoutParams = defImage.getLayoutParams();
        missingImage = defImage.getDrawable();

        pictures = new ArrayList<>();

        if (listImagesFilenames.size() != 0)
        {
            museeItem_scrollLayout.removeView(defImage);
            for (int i = 0; i < listImagesFilenames.size(); i++)
            {
                ImageView img = new ImageView(this);
                img.setImageDrawable(missingImage);
                img.setScaleType(ImageView.ScaleType.FIT_CENTER);

                TextView text = new TextView(this);

                ImageAndDescription picture = new ImageAndDescription(this,img,text);
                pictures.add(picture);
            }
        }
        else
            defImage.setImageResource(R.drawable.kronk);    // Oh right.... The picture of the item, the item's picture. Best meme.
    }

    // The picture has been painted, frame it, hang it on the museum's wall, don't forget the fancy captation bellow too.
    public void pictureReceived(String filename)
    {
        pictures.get(listImagesFilenames.getPosition(filename)).image.setImageDrawable(MuseeDataHandler.getPicture(item,filename));
        pictures.get(listImagesFilenames.getPosition(filename)).text.setText(MuseeDataHandler.getPictureDescription(item,filename));
    }

    // Hold my paintings please.. they're heavy.
    private class ImageAndDescription
    {
        public LinearLayout layout;
        public ImageView image;
        public TextView text;
        public ImageAndDescription(Context context, ImageView image, TextView text)
        {
            this.image = image;
            this.text = text;
            image.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 800));
            ViewGroup.LayoutParams textParams = new ViewGroup.LayoutParams(1000, ViewGroup.LayoutParams.WRAP_CONTENT);
            text.setLayoutParams(textParams);
            text.setGravity(Gravity.CENTER);
            text.setText("");

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(10,0,10,0);
            layout = new LinearLayout(context);
            museeItem_scrollLayout.addView(layout);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(layoutParams);
            layout.addView(image);
            layout.addView(text);
        }
    }
}

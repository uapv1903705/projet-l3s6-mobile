package fr.uavignon.racamond.projetl3;

import java.text.Normalizer;

// Used to easily compare items with the searchbar.
public class SearchFilter
{
    String filter;

    public SearchFilter()
    {
        filter = new String();
    }

    public void set(String name)
    {
        this.filter = normalize(name);
    }

    public String toString()
    {
        return filter;
    }

    public boolean isEmpty()
    {
        return filter.isEmpty();
    }

    // Does it matches the filter?
    public boolean checkMatch(String s)
    {
        if(filter.isEmpty())
            return true;
        if(normalize(s).contains(filter))
            return true;
        return false;
    }

    // I don't need those fancy accents and uppercase stuff, it's bad!
    private String normalize(String original)
    {
        String s = ""+original; // Don't copy the ref
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[^\\p{ASCII}]", "");
        s = s.toLowerCase();
        return s;
    }
}
